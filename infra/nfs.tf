# ---------------------------------------------- #
#                  Deploy NFS VM                 #
# ---------------------------------------------- #

# Create NFS instance NFS01
resource "vkcs_compute_instance" "nfs01" {
  name      = var.nfs01_name
  flavor_id = data.vkcs_compute_flavor.nfs.id
  security_groups = [
    vkcs_networking_secgroup.nfs.name
  ]
  image_name        = data.vkcs_images_image.ubuntu.name
  availability_zone = "GZ1"
  key_pair          = var.keypair_name
  config_drive      = true
  user_data         = templatefile("../product/nfs-init.sh", {
    NODE_VERSION = var.node_exporter_version
  })

  block_device {
    uuid                  = data.vkcs_images_image.ubuntu.id
    source_type           = "image"
    destination_type      = "volume"
    volume_type           = var.nfs_volume_type
    volume_size           = var.nfs_volume_size
    boot_index            = 0
    delete_on_termination = true
  }

  network {
    uuid        = vkcs_networking_network.project_network.id
    fixed_ip_v4 = "10.0.1.105"
  }
  depends_on = [
    vkcs_networking_router_interface.router_interface
  ]
}
