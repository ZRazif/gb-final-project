# -------------------------------- #
#       Authentication data        #
# -------------------------------- #
variable "cloudflare_api_token" {
  default = ""
}

variable "cloudflare_account_id" {
  default = ""
}

# -------------------------------- #
#       General data block         #
# -------------------------------- #
# Search for image ID
data "vkcs_images_image" "ubuntu" {
  name        = "Ubuntu-20.04.1-202008"
  most_recent = true

  properties = {
    key = "value"
  }
}

variable "domain_name" {
  default = ""
}

variable "prometheus_version" {
  default = "2.43.0-rc.0"
}

variable "grafana_version" {
  default = "9.4.3"
}

variable "alertmanager_version" {
  default = "0.25.0"
}

variable "node_exporter_version" {
  default = "1.5.0"
}

variable "nginx_exporter_version" {
  default = "0.11.0"
}

# -------------------------------- #
#        Telegram auth data        #
# -------------------------------- #

variable "bot_chat" {
  default = ""
}

variable "bot_token" {
  default = ""
}

# -------------------------------- #
#       Wordpress auth data        #
# -------------------------------- #
variable "wp_database" {
  default = ""
}

variable "wp_username" {
  default = ""
}

variable "wp_password" {
  default = ""
}

# -------------------------------- #
#    General project variables     #
# -------------------------------- #
# Defining Keypair name
variable "keypair_name" {
  default = "project-keypair"
}

# -------------------------------- #
#         Network variables        #
# -------------------------------- #
# Defining Private Network name
variable "network_name" {
  default = "project-network"
}

# Search for public network Floating IPs
data "vkcs_networking_network" "ext_network" {
  name = "ext-net"
}

# Defining subnet name
variable "subnet_name" {
  default = "project-subnet"
}

# Defining subnet CIDR
variable "subnet_cidr" {
  default = "10.0.1.0/24"
}

# Defining project router name
variable "project_router" {
  default = "project-router"
}

# -------------------------------- #
#       Loadbalancer variables     #
# -------------------------------- #
# Defining Loadbalancer name
variable "lb_name" {
  default = "project_lb"
}

# Defining Loadbalancer listener name
variable "lb_listener_name" {
  default = "project_lb_listener"
}

# Defining Loadbalancer listener proto
variable "lb_listener_proto" {
  default = "HTTP"
}

# Defining Loadbalancer listener port
variable "lb_listener_port" {
  default = 80
}

# Defining Loadbalancer pool name
variable "lb_pool_name" {
  default = "project_lb_pool"
}

# Defining Loadbalancer pool proto
variable "lb_pool_proto" {
  default = "HTTP"
}

# Defining Loadbalancer pool method
variable "lb_method" {
  default = "ROUND_ROBIN"
}

# -------------------------------- #
#       APP servers variables      #
# -------------------------------- #
# Search for APP server flavor
data "vkcs_compute_flavor" "app" {
  name = "Basic-1-2-20"
}

# Defining APP01 server hostname
variable "app01_name" {
  default = "app01"
}

# Defining APP02 server hostname
variable "app02_name" {
  default = "app02"
}

# Defining APP servers volume type
variable "app_volume_type" {
  default = "ceph-ssd"
}

# Defining APP servers volume size
variable "app_volume_size" {
  default = 20
}

# -------------------------------- #
#       DBaaS servers variables    #
# -------------------------------- #

# Search for available flavors for MySQL DBaaS
data "vkcs_compute_flavor" "dbaas" {
  name = "Standard-2-8-50"
}

# Defining DB01 server hostname
variable "db01_name" {
  default = "db01"
}

# Defining DB servers volume type
variable "db_volume_type" {
  default = "ceph-ssd"
}

# Defining DB servers volume size
variable "db_volume_size" {
  default = 20
}

# -------------------------------- #
#       NFS servers variables      #
# -------------------------------- #

# Search for NFS server flavor
data "vkcs_compute_flavor" "nfs" {
  name = "Basic-1-2-20"
}

# Defining NFS01 server hostname
variable "nfs01_name" {
  default = "nfs01"
}

# Defining NFS servers volume type
variable "nfs_volume_type" {
  default = "ceph-ssd"
}

# Defining NFS servers volume size
variable "nfs_volume_size" {
  default = 20
}

# -------------------------------- #
#       MON servers variables      #
# -------------------------------- #
# Search for MON server flavor
data "vkcs_compute_flavor" "mon" {
  name = "Basic-1-2-20"
}

# Defining MON01 server hostname
variable "mon01_name" {
  default = "mon01"
}

# Defining MON servers volume type
variable "mon_volume_type" {
  default = "ceph-ssd"
}

# Defining MON servers volume size
variable "mon_volume_size" {
  default = 20
}
