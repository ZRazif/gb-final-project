# ---------------------------------------------- #
#                 Deploy LBaaS                   #
# ---------------------------------------------- #

# Create Loadbalancer
resource "vkcs_lb_loadbalancer" "project_lb" {
  name          = var.lb_name
  vip_subnet_id = vkcs_networking_subnet.project_subnet.id
  depends_on = [
    vkcs_compute_instance.app01,
    vkcs_compute_instance.app02
  ]
}

# Create Loadbalancer floating IP
resource "vkcs_networking_floatingip" "project_lb_fip" {
  pool = data.vkcs_networking_network.ext_network.name
}

# Create Loadbalancer listener
resource "vkcs_lb_listener" "project_lb_listener" {
  name            = var.lb_listener_name
  protocol        = var.lb_listener_proto
  protocol_port   = var.lb_listener_port
  loadbalancer_id = vkcs_lb_loadbalancer.project_lb.id
}

# Create Loadbalancer pool
resource "vkcs_lb_pool" "project_lb_pool" {
  name        = var.lb_pool_name
  protocol    = var.lb_pool_proto
  lb_method   = var.lb_method
  listener_id = vkcs_lb_listener.project_lb_listener.id
}

# Create Loadbalancer pool member 1
resource "vkcs_lb_member" "member_1" {
  address       = vkcs_compute_instance.app01.access_ip_v4
  protocol_port = 80
  pool_id       = vkcs_lb_pool.project_lb_pool.id
  subnet_id     = vkcs_networking_subnet.project_subnet.id
  weight        = 1
#  depends_on = [
#    vkcs_compute_instance.app01
#  ]
}

# Create Loadbalancer pool member 1
resource "vkcs_lb_member" "member_2" {
  address       = vkcs_compute_instance.app02.access_ip_v4
  protocol_port = 80
  pool_id       = vkcs_lb_pool.project_lb_pool.id
  subnet_id     = vkcs_networking_subnet.project_subnet.id
  weight        = 1
#  depends_on = [
#    vkcs_compute_instance.app02
#  ]
}
/*
# Adding health checks to Loadbalancer
resource "vkcs_lb_monitor" "monitor_1" {
  pool_id        = vkcs_lb_pool.project_lb_pool.id
  type           = "HTTP"
  expected_codes = 200
  http_method    = "GET"
  url_path       = "/"
  delay          = 600
  timeout        = 300
  max_retries    = 10
}
*/
# Associate Loadbalancer floating IP
resource "vkcs_networking_floatingip_associate" "lb_fip" {
  floating_ip = vkcs_networking_floatingip.project_lb_fip.address
  port_id     = vkcs_lb_loadbalancer.project_lb.vip_port_id
}
