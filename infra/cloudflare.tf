# ---------------------------------------------- #
#         Deploy Cloudflare domain zone          #
# ---------------------------------------------- #

resource "cloudflare_zone" "project_domain" {
  account_id = var.cloudflare_account_id
  zone       = var.domain_name
  plan       = "free"
  jump_start = "true"
}

resource "cloudflare_record" "A_record_lb" {
  zone_id = cloudflare_zone.project_domain.id
  name    = var.domain_name
  value   = vkcs_networking_floatingip.project_lb_fip.address
  type    = "A"
  ttl     = 1
  proxied = "true"
  depends_on = [
    vkcs_networking_floatingip.project_lb_fip
  ]
}

resource "cloudflare_record" "CNAME_lb" {
  zone_id = cloudflare_zone.project_domain.id
  name    = "www"
  value   = var.domain_name
  type    = "CNAME"
  ttl     = 1
  proxied = "true"
  depends_on = [
    vkcs_networking_floatingip.project_lb_fip
  ]
}

resource "cloudflare_record" "A_record_mon" {
  zone_id = cloudflare_zone.project_domain.id
  name    = "mon.${var.domain_name}"
  value   = vkcs_networking_floatingip.mon_fip.address
  type    = "A"
  ttl     = 1
  proxied = "true"
  depends_on = [
    vkcs_compute_floatingip_associate.mon_fip
  ]
}
